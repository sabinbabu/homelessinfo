/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homelessinfo;

/**
 *
 * @author preety
 */
public enum AGE {
    // declare constants of enum type                                
    UNDER55("50-54"),
    BELOW60("55-59"),
    BELOW65("60-64"),
    OVER65("over 65");

    private final String AGERANGE;

    // enum constructor
    AGE(String AGERANGE) {
        this.AGERANGE = AGERANGE;

    }

    // accessor for field AgeRange
    public String getAgerange() {
        return AGERANGE;
    }

    @Override
    public String toString() {
        return String.format("%-10s", this.getAgerange());
    }
    //Override enum agerange object with toString

} // end enum Age

