/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homelessinfo;

/**
 *
 * @author preety
 */
public abstract class Person {

    private final String age;
    private final String gender;

    /**
     *
     * @return
     */
    public Person(String agegroup, String gendergroup) {
        this.age = agegroup;
        this.gender = gendergroup;
    }//parameterised constructor

    public Person(Person another) {
        this.age = another.age;
        this.gender = another.gender;
    }//copy constructor

    @Override
    public String toString() {
        return String.format("%-10s %-10s", age, gender);

    }

}
