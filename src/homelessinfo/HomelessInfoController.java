/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package homelessinfo;

import static homelessinfo.DataFile.riskyPersons;
import static homelessinfo.DataFile.sa3tenantlist;
import java.io.IOException;
import static java.lang.System.exit;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.scene.control.TextArea;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author preety @StudentId:12121267 /*** Controller class for HomelessInfo to
 * run **
 */
public class HomelessInfoController implements Initializable {

    @FXML
    private TextField txtlocation;
    @FXML
    private TextField txtsa3code;
    @FXML
    private ComboBox txtagecategory;
    @FXML
    private ComboBox txtincomecategory;

    @FXML
    private ComboBox ComboGender;
    @FXML
    private ComboBox CmbIncomeSrc;
    @FXML
    private TabPane tab_id;
    @FXML
    private Button location_submit;
    private TextField txt_sa3code;

    @FXML
    private TextField txtRiskyPerson;

    private ComboBox ComboGender1;
    @FXML
    private ComboBox CmbSa3Code;
    private Label lblGender1;

    DataFile datalist = new DataFile();
    Location locationobj = new Location();
    LinkedList<RiskyPersons> riskypersonlist = datalist.riskyPersons;
    LinkedList<Location> locationlist = datalist.locationlist;
    DatabaseUtility databasecon = new DatabaseUtility();
    @FXML
    private ComboBox cmbSa3code;
    @FXML
    private Label lbl_locationerr;
    @FXML
    private Button CmbAll;
    private Label lblCount1;
    private Label lblsa3code11;
    @FXML
    private TextArea txtarea;
    @FXML
    private Button btnExit;
    @FXML
    private Button CmbAll1;
    @FXML
    private Button CmbAll11;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        System.out.println("initi");
        this.txtagecategory.getItems().addAll("50-54", "55-59", "60-64", "over 65");
        this.txtincomecategory.getItems().addAll("Negative/Nil income", "$1-$399", "$400-$599", "$600-$999");
        this.ComboGender.getItems().addAll("Male", "Female");
        this.CmbIncomeSrc.getItems().addAll("Employed", "Other");
        // this.ComboGender1.getItems().addAll("Male", "Female");

        datalist.openFile();
        try {
            datalist.readRecords();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }

    @FXML
    private void saveLocation(ActionEvent event) throws IOException {

        String txt_sa3code = txtsa3code.getText();
        String txt_location = txtlocation.getText();
        if (txt_sa3code.length() > 0 && txt_location.length() > 0) {

            if (txt_sa3code.charAt(0) == '5' && txt_sa3code.length() == 5 && txt_sa3code.matches("[0-9]+")) {
                if (txt_location.matches("[a-zA-Z -]+")) {

                    locationobj = new Location(txt_sa3code.trim(), txt_location.trim());
                    locationlist.add(locationobj);
                    if (databasecon.addlocation(txt_sa3code.trim(), txt_location.trim())) {
                        JOptionPane.showMessageDialog(null, "Location Successfully Saved.");
                        this.tab_id.getSelectionModel().selectNext();

                        this.clearFields();
                    } else {
                        JOptionPane.showMessageDialog(null, "Location already exists.");

                    }

                } else {
                    JOptionPane.showMessageDialog(null, "Location can only be Alphabets.");

                }

            } else {
                JOptionPane.showMessageDialog(null, "SA3Code must start with 5 and of 5 digits only.");
            }

        } else {
            JOptionPane.showMessageDialog(null, "SA3Code or Location not entered.");

        }

    }

    @FXML
    private void saveTenant(ActionEvent event) {

        if (this.txtagecategory.getValue() != null) { //if count of txtagecategory is not null
            if (this.txtincomecategory.getValue() != null) { //if count of txtincomecategory is not null
                if (this.ComboGender.getValue() != null) { //if count of ComboGender is not null

                    if (this.CmbIncomeSrc.getValue() != null) { //if count of CmbIncomeSrc is not null
                        if (this.cmbSa3code.getValue() != null) { //if count of cmbSa3code is not null
                            if (this.txtRiskyPerson.getText().length() > 0 && this.txtRiskyPerson.getText().matches("[0-9]+")) { //if count of txtRiskyPerson is not null
                                String AgeCategoryVal = (String) this.txtagecategory.getValue();
                                AGE EnumAge = AGE.valueOf(datalist.getAgeValue(AgeCategoryVal.trim()));
                                String IncomeCategoryVal = (String) this.txtincomecategory.getValue();
                                WEEKLY_INCOME EnumIncomeCategory = WEEKLY_INCOME.valueOf(datalist.getIncomeValue(IncomeCategoryVal.trim()));
                                String GenderVal = (String) this.ComboGender.getValue();
                                GENDER EnumGender = GENDER.valueOf(GenderVal.trim());
                                String IncomeSourceVal = (String) this.CmbIncomeSrc.getValue();
                                String SA3CodeVal = (String) this.cmbSa3code.getValue();
                                int RiskyPersonVal = Integer.parseInt(this.txtRiskyPerson.getText());
                                int locationid = 0;
                                int ageid = 0;
                                int weekly_incomeid = 0;
                                int tenantid = 0;
                                locationid = databasecon.getLocationid(SA3CodeVal.trim());
                                ageid = databasecon.getAgebyid(AgeCategoryVal.trim());
                                weekly_incomeid = databasecon.getWeekly_Incomeid(IncomeCategoryVal.trim());
                                for (RiskyPersons items : riskypersonlist) {
                                    if (items.toString().contains(EnumAge.toString()) && items.toString().contains(EnumIncomeCategory.toString())
                                            && items.toString().contains(EnumIncomeCategory.toString()) && items.toString().contains(EnumGender.toString())
                                            && items.toString().contains(IncomeSourceVal.trim()) && items.toString().contains(SA3CodeVal.trim())) {
                                        items.setCountofPerson(items.getCountofPerson() + RiskyPersonVal);
                                        tenantid = databasecon.getSa3Tenantid(ageid, weekly_incomeid, GenderVal.trim(), locationid, IncomeSourceVal.trim());
                                        databasecon.updateRiskyCount(items.getCountofPerson() + RiskyPersonVal, tenantid);
                                        System.out.println(tenantid);
                                        break;
                                    }

                                }//update the count of riskyPerson as entered by user
                                Location locationobj1 = new Location(); //create new instance of location
                                for (Location locationitems : locationlist) {

                                    if (locationitems.getSa3code().contains(SA3CodeVal.trim())) {
                                        locationobj1 = locationitems;
                                        System.out.println(locationobj1.toString());
                                        break;

                                    }
                                }
                                SA3TenantCategory sa3TenantCategory = new SA3TenantCategory(AgeCategoryVal, GenderVal, locationid, IncomeCategoryVal,
                                        IncomeSourceVal); //create new instance of SA3TenantCategory
                                tenantid = databasecon.addTenantCategory(ageid, weekly_incomeid, GenderVal.trim(), locationid, IncomeSourceVal.trim());
                                RiskyPersons riskyPerson = new RiskyPersons(tenantid, RiskyPersonVal
                                ); //create new instance of RiskyPersons
                                databasecon.addRiskyCount(tenantid, RiskyPersonVal);
                                System.out.println("Risky Count Added");

                                sa3tenantlist.add(sa3TenantCategory); //add sa3tenantCategory to arraylist sa3TenantCategory
                                riskyPersons.add(riskyPerson); //add riskyPerson to arraylist riskyPersons
                                System.out.println(riskyPerson.toString());
                                JOptionPane.showMessageDialog(null, "Tenant Successfully Saved.");
                                this.tab_id.getSelectionModel().selectNext();

                            }//if count of riskyPerson is not null
                            else {
                                JOptionPane.showMessageDialog(null, "Risky Person Count not valid.");

                            }
                        }//if sa3code not null ends
                        else {
                            JOptionPane.showMessageDialog(null, "SA3Code not entered.");

                        }
                    }//IfIncomeSOurce not null ends
                    else {
                        JOptionPane.showMessageDialog(null, "Income Source not entered.");

                    }
                } //If Gender not null ends
                else {
                    JOptionPane.showMessageDialog(null, "Gender not entered.");

                }
            } //If Income Categroy not null
            else {
                JOptionPane.showMessageDialog(null, "Income Category not entered.");

            }

        }//if AgeCategory not null ends
        else {
            JOptionPane.showMessageDialog(null, "Age not entered.");

        }

    }//end of saveTenant Method

    @FXML
    private void generateReport(ActionEvent event) throws SQLException {
        txtarea.setText("");
        String result = "";
        String sa3code = (String) this.CmbSa3Code.getValue();
        if (sa3code != null) {
            LinkedList<RiskyPersons> people = databasecon.getAllRiskyLocation(sa3code.trim());
            for (RiskyPersons riskylist : people) {
                LinkedList<SA3TenantCategory> tenantlist = databasecon.getAllTenants(riskylist.getsa3tenantcategory());
                for (SA3TenantCategory items : tenantlist) {
                    int locationid = items.getlocation();
                    LinkedList<Location> locationlist = databasecon.getAllLocation(locationid);
                    for (Location locations : locationlist) {
                        result = result + locations.toString() + items.toString() + riskylist.getCountofPerson() + "\n";
                    }
                }

            }
            txtarea.setText(result);
        } else {
            JOptionPane.showMessageDialog(null, "Sa3code not entered.");
        }

    }//calculate the count of riskyPersons based on user input

    @FXML
    private void getAllCount() throws SQLException {
        this.getByGender("%male%");
    }
    //method to count the total number of riskyPersons

    private void clearFields() {
        txtsa3code.clear();//clear the values of txtsa3code
        txtlocation.clear();//clear the values of txtlocation
    }

    private void clearvalues() {
        ComboGender1.valueProperty().set(null); //clear the values of ComboGender1
        CmbSa3Code.valueProperty().set(null); //clear the values of CmbSa3Code
        cmbSa3code.valueProperty().set(null);
    }

    @FXML
    private void btnExit(ActionEvent event) {
        datalist.closeFile();
        exit(1);
    }

    private void getByGender(String gender) throws SQLException {
        txtarea.setText("");
        LinkedList<RiskyPersons> people = databasecon.getAllGender(gender);
        String text = "";
        if (people.size() > 0) { // display all entries

            for (RiskyPersons item : people) {
                int tenantid = item.getsa3tenantcategory();
                LinkedList<SA3TenantCategory> tenantlist = databasecon.getAllTenants(tenantid);
                for (SA3TenantCategory items : tenantlist) {
                    int locationid = items.getlocation();
                    LinkedList<Location> locationlist = databasecon.getAllLocation(locationid);
                    for (Location locations : locationlist) {
                        text = text + locations.toString() + items.toString() + item.getCountofPerson() + "\n";
                    }

                }

            }
            txtarea.setText(text);
        } else {
            JOptionPane.showMessageDialog(null, "Gender not entered.");
        }
    }

    @FXML
    private void getAllMales(ActionEvent event) throws SQLException {
        this.getByGender("male");
    }

    @FXML
    private void getAllFemales(ActionEvent event) throws SQLException {
        this.getByGender("female");
    }

    @FXML
    private void getAllSa3code(Event event) {
        if (CmbSa3Code.getValue() != null) {
            CmbSa3Code.getItems().removeAll();

        }
        if ((cmbSa3code.getValue() != null)) {
            cmbSa3code.getItems().removeAll();
        }
        ObservableList<String> sa3list = databasecon.getSa3Code();

        CmbSa3Code.setItems(FXCollections.observableArrayList(sa3list));
        cmbSa3code.setItems(FXCollections.observableArrayList(sa3list));

    }

}
