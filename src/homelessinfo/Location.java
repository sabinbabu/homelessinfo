/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homelessinfo;

/**
 *
 * @author preety
 */
public class Location {

    private String sa3code;//instantiate sa3code variable
    private String location;//instantiate location variable

    public Location() {
        //default constructor
    }

    public Location(String sa3code, String location) {
        this.sa3code = sa3code;
        this.location = location;
    }//parameterised constructor

    public Location(Location another) {
        this.sa3code = another.sa3code;
        this.location = another.location;
    }//copy constructor

    public String getSa3code() {
        return sa3code;
    }//getter method for sa3code

    public void setSa3code(String sa3code) {
        this.sa3code = sa3code;
    }//setter method for sa3code

    public String getlocation() {
        return location;
    }//getter method for location

    public void setlocation(String location) {
        this.location = location;
    }//setter method for location

    @Override
    public String toString() {
        return String.format("%-10s %-40s", this.getSa3code(), this.getlocation());
    }//override object toString method

}
