/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homelessinfo;

/**
 *
 * @author preety
 */
public class RiskyPersons {

    private int CountofPerson;
    private int sa3tenantcategory;

    public RiskyPersons(int sa3tenantcategory, int CountofPerson) {

        this.sa3tenantcategory = sa3tenantcategory;
        this.CountofPerson = CountofPerson;

    } //Parameterised COnstructor

    public void setSa3tenantCategory(int sa3tenantcategory) {
        this.sa3tenantcategory = sa3tenantcategory;
    }

    public int getsa3tenantcategory() {
        return this.sa3tenantcategory;
    }

    public void setCountofPerson(int CountofPerson) {
        this.CountofPerson = CountofPerson;
    }

    public int getCountofPerson() {
        return this.CountofPerson;
    }
    // return String representation of HourlyEmployee object              

    @Override
    public String toString() {
        return String.format("%d %-20d%n",
                sa3tenantcategory, getCountofPerson());
    }

}
