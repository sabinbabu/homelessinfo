/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package homelessinfo;

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
import static java.lang.System.exit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.swing.JOptionPane;

/**
 *
 * @author preety
 * DatabaseUtility.java:::This class is for saving and reading data from database. 
 * 
 */
public class DatabaseUtility {

    private static final String URL = "jdbc:mysql://localhost:3306/new_schema";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "Nepal@123";

    private Connection connection; // manages connection
    private PreparedStatement createtablelocation; //creates a table (location)
    private PreparedStatement insertNewLocation; //inserts a new location
    private PreparedStatement getLocationid; //get location ID
    private PreparedStatement getLocationbyid;// get location name
    private PreparedStatement getsa3code; //gets sa3code
    private PreparedStatement createtableAgeGroup; //creates a table (age group)
    private PreparedStatement insertAgeData; //for inserting age data
    private PreparedStatement getAgeid; //get age id
    private PreparedStatement createtableWeekly_income; //creates a table (weekly income)
    private PreparedStatement insertWeekly_income; //for inserting weekly income
    private PreparedStatement getweekly_incomeid; // for getting income id
    private PreparedStatement createtableSa3Category; //creates a table (sa3 category)
    private PreparedStatement insertSa3Category; //for insertign sa3 category
    private PreparedStatement getSa3categoryId; // for getting sa3 category
    private PreparedStatement getAllSa3Category; // for getting all sa3category
    private PreparedStatement updateRiskyCount; //for updating risky count
    private PreparedStatement createtableRiskyCount; //creates a table (Risky count)
    private PreparedStatement getLocationbysa3code; // getting location by sa3code
    private PreparedStatement getTenantBylocation; //getting tenant by location
    private PreparedStatement insertRiskyCount; //inserting risky count
    private PreparedStatement getallmalesdesc; //get data of all males in descending order
    private PreparedStatement getallfemalesdesc; //get data of all females by descending order
    private PreparedStatement getriskycountbytenant; //getting risky count by tenant
    private ResultSet rs; //result set

    // constructor
    public DatabaseUtility() {
        try {
            connection
                    = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            createtablelocation = connection.prepareStatement("CREATE TABLE IF NOT EXISTS Location "
                    + "(locationId INTEGER not NULL AUTO_INCREMENT,"
                    + "sa3code VARCHAR(50),"
                    + "location VARCHAR(100),"
                    + "PRIMARY KEY (locationId) ,"
                    + "UNIQUE INDEX (sa3code))");
            //create table Location
            insertNewLocation = connection.prepareStatement(
                    "INSERT INTO location "
                    + "(sa3code, location) "
                    + "VALUES (?, ?)");
            // create insert that adds a new entry into the database
            getLocationid = connection.prepareStatement(
                    "SELECT locationId from Location where sa3code LIKE ? LIMIT 1");
            getsa3code = connection.prepareStatement("Select distinct sa3code from location group by sa3code");
            //get location from Id
            getLocationbyid = connection.prepareStatement(
                    "SELECT sa3code, location from Location where locationid = ? ");
            getLocationbysa3code = connection.prepareStatement(
                    "SELECT sa3code, location from Location where sa3code LIKE ? ");
            createtableAgeGroup = connection.prepareStatement("CREATE TABLE IF NOT EXISTS Age "
                    + "(AgeId INTEGER not NULL AUTO_INCREMENT,"
                    + "agegroup VARCHAR(50),"
                    + "PRIMARY KEY (AgeId),"
                    + "UNIQUE INDEX (agegroup))");
            //create table Age
            insertAgeData = connection.prepareStatement(
                    "INSERT INTO Age "
                    + "(agegroup) "
                    + "VALUES (?)");
            getAgeid = connection.prepareStatement(
                    "SELECT AgeId from age where agegroup like ?");
            // create insert that adds a new entry into the database
            createtableWeekly_income = connection.prepareStatement("CREATE TABLE IF NOT EXISTS Weekly_income "
                    + "(Weekly_incomeId INTEGER not NULL AUTO_INCREMENT,"
                    + "incomegroup VARCHAR(50),"
                    + "PRIMARY KEY (Weekly_incomeId) ,"
                    + "UNIQUE INDEX (incomegroup))");
            //create table weekly_income
            insertWeekly_income = connection.prepareStatement(
                    "INSERT INTO Weekly_income "
                    + "(incomegroup) "
                    + "VALUES (?)");
            getweekly_incomeid = connection.prepareStatement(
                    "SELECT Weekly_incomeId from Weekly_income where incomegroup LIKE ?");
            // create insert that adds a new entry into the database
            createtableSa3Category = connection.prepareStatement("CREATE TABLE IF NOT EXISTS Sa3Category "
                    + "(Sa3CategoryId INTEGER not NULL AUTO_INCREMENT,"
                    + "AgeId INTEGER,"
                    + "Weekly_incomeId INTEGER,"
                    + "Gender VARCHAR(10),"
                    + "LocationId INTEGER,"
                    + "Income_Source VARCHAR(50),"
                    + "PRIMARY KEY (Sa3CategoryId) )");
            //create table SA3Category
            insertSa3Category = connection.prepareStatement(
                    "INSERT INTO Sa3Category "
                    + "(AgeId,Weekly_incomeId,Gender,locationId,Income_Source) "
                    + "VALUES (?, ?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            //get sa3categoryid
            getSa3categoryId = connection.prepareStatement("SELECT Sa3CategoryId from sa3category where (AgeId= ?)"
                    + " AND (Weekly_incomeId=? )" + "AND (Gender like ?) "
                    + "AND (LocationId=? AND Income_Source like ? ) LIMIT 1");
            // create insert that adds a new entry into the database
            getAllSa3Category = connection.prepareStatement(
                    "SELECT agegroup, incomegroup,gender, locationid, income_source from sa3category inner join age on age.ageid=sa3category.AgeId\n"
                    + "inner join weekly_income on weekly_income.Weekly_incomeId=sa3category.Weekly_incomeId "
                    + "where sa3categoryid = ? ");
            getTenantBylocation = connection.prepareStatement(
                    "SELECT agegroup, incomegroup,gender, location.locationid, income_source from sa3category inner join age on age.ageid=sa3category.AgeId "
                    + "inner join weekly_income on weekly_income.Weekly_incomeId=sa3category.Weekly_incomeId "
                    + "inner join location on location.locationid=sa3category.locationid "
                    + "where location.sa3code like ? ");
            createtableRiskyCount = connection.prepareStatement("CREATE TABLE IF NOT EXISTS RiskyCount "
                    + "(RiskyId INTEGER not NULL AUTO_INCREMENT,"
                    + "Sa3CategoryID INTEGER,"
                    + "Count INTEGER,"
                    + "PRIMARY KEY (RiskyId) )");
            //create table Age
            insertRiskyCount = connection.prepareStatement(
                    "INSERT INTO RiskyCount "
                    + "(Sa3CategoryID, Count) "
                    + "VALUES (?,?)");
            // create insert that adds a new entry into the database
            updateRiskyCount = connection.prepareStatement("Alter table RiskyCount set Count=? where Sa3CategoryID=?");
            // create query that selects all entries in the AddressBook
            getallmalesdesc = connection.prepareStatement("SELECT riskycount.sa3categoryid, count FROM sa3category"
                    + " inner join RiskyCount on sa3category.sa3categoryid=riskycount.sa3categoryid "
                    + " inner join Weekly_income on weekly_income.weekly_incomeID= sa3category.weekly_incomeid "
                    + " inner join location on location.locationid=sa3category.locationid "
                    + " where sa3category.gender LIKE ? " + " ORDER BY weekly_income.weekly_incomeId DESC ");
            //get all male details by income category (descending order)
            getallfemalesdesc = connection.prepareStatement("SELECT riskycount.sa3categoryid, count FROM sa3category"
                    + " inner join RiskyCount on sa3category.sa3categoryid=riskycount.sa3categoryid "
                    + " inner join Weekly_income on weekly_income.weekly_incomeID= sa3category.weekly_incomeid "
                    + " inner join Age on Age.AgeID= sa3category.ageid "
                    + " inner join location on location.locationid=sa3category.locationid "
                    + " where sa3category.gender LIKE ? " + " ORDER BY age.ageId DESC ");
            //get all female details by age category (descending order)

            getriskycountbytenant = connection.prepareStatement("SELECT riskycount.sa3categoryid, riskycount.count FROM location"
                    + " inner join sa3category on sa3category.locationid=location.locationid "
                    + " inner join riskycount on riskycount.sa3categoryid=sa3category.sa3categoryid "
                    + " where sa3code LIKE ? ");

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            System.exit(1);
        }
    }

    public void createAllTable() {
        try {
            createtablelocation.executeUpdate();
            createtableAgeGroup.executeUpdate();
            createtableWeekly_income.executeUpdate();
            createtableSa3Category.executeUpdate();
            createtableRiskyCount.executeUpdate();
            this.addAge("50-54");
            this.addAge("55-59");
            this.addAge("60-64");
            this.addAge("over 65");
            this.addWeekly_Income("Negative/Nil income");
            this.addWeekly_Income("$1-$399");
            this.addWeekly_Income("$400-$599");
            this.addWeekly_Income("$600-$999");
            System.out.println("table location created");
        } catch (SQLException sqlException) {
            if (sqlException instanceof SQLIntegrityConstraintViolationException) {
                JOptionPane.showMessageDialog(null, "Record already exists.");
                // Duplicate entry
            } else {
                System.out.println(sqlException);

            }
        }
    }

    // add data in Location Table
    public boolean addlocation(String sa3code, String location) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            insertNewLocation.setString(1, sa3code);
            insertNewLocation.setString(2, location);
            insertNewLocation.executeUpdate();
            System.out.println("Location Added Successfully");
            return true;
        } catch (SQLException sqlException) {
            if (sqlException instanceof SQLIntegrityConstraintViolationException) {
                JOptionPane.showMessageDialog(null, "Location already exists.");
                // Duplicate entry
            } else {
                System.out.println(sqlException);

            }
            return false;
        }
    }

    public int getLocationid(String sa3code) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            getLocationid.setString(1, sa3code);
            rs = getLocationid.executeQuery();
            if (rs.next()) {
                System.out.println(rs.getInt("locationId"));
                int locationid = rs.getInt("locationId");
                //System.out.println(rs.getInt("locationId"));
                return locationid;
            } else {
                return 0;
            }

        } catch (SQLException sqlException) {
            System.out.println(sqlException);
            return 0;

        }
    }

    public ObservableList<String> getSa3Code() {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            rs = getsa3code.executeQuery();
            ObservableList<String> results = FXCollections.observableArrayList();
            while (rs.next()) {
                results.add(rs.getString("sa3code"));
                //System.out.println(rs.getInt("locationId"));
            }
            return results;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();

        }
        return null;
    }

    // select all of the addresses in the database
    public LinkedList<Location> getAllLocation(int locationid) throws SQLException {
        // executeQuery returns ResultSet containing matching entries
        getLocationbyid.setInt(1, locationid);
        try (ResultSet resultSet = getLocationbyid.executeQuery()) {
            LinkedList<Location> results = new LinkedList<>();

            while (resultSet.next()) {
                results.add(new Location(
                        resultSet.getString("Sa3code"),
                        resultSet.getString("location")
                ));
            }

            return results;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return null;
    }

    public LinkedList<Location> getAllLocationBySa3code(String sa3code) throws SQLException {
        // executeQuery returns ResultSet containing matching entries
        getLocationbysa3code.setString(1, sa3code.trim());
        try (ResultSet resultSet = getLocationbysa3code.executeQuery()) {
            LinkedList<Location> results = new LinkedList<>();

            while (resultSet.next()) {
                results.add(new Location(
                        resultSet.getString("Sa3code"),
                        resultSet.getString("location")
                ));
            }

            return results;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return null;
    }

    // add data in Age
    public void addAge(String agegroup) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            insertAgeData.setString(1, agegroup);
            insertAgeData.executeUpdate();

        } catch (SQLException sqlException) {
            System.out.println(sqlException);

        }
    }

    public int getAgebyid(String agegroup) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            getAgeid.setString(1, agegroup);
            rs = getAgeid.executeQuery();
            if (rs.next()) {
                int Ageid = rs.getInt("AgeId");
                return Ageid;
            } else {
                return 0;
            }

        } catch (SQLException sqlException) {
            System.out.println(sqlException);
            return 0;

        }
    }

    // add data in Weekly_Income
    public void addWeekly_Income(String weekly_income) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            insertWeekly_income.setString(1, weekly_income);
            insertWeekly_income.executeUpdate();
            System.out.println("Weekly_income Added Succesfully");

        } catch (SQLException sqlException) {
            System.out.println(sqlException);

        }
    }

    public int getWeekly_Incomeid(String weekly_income) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            getweekly_incomeid.setString(1, weekly_income);
            rs = getweekly_incomeid.executeQuery();
            if (rs.next()) {
                int weekly_incomeid = rs.getInt("Weekly_incomeId");
                return weekly_incomeid;
            } else {
                return 0;
            }

        } catch (SQLException sqlException) {
            System.out.println(sqlException);

            return 0;

        }
    }

    // add data in Location Table
    public int addTenantCategory(int ageid, int weekly_income, String gender, int location, String income_source) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            insertSa3Category.setInt(1, ageid);
            insertSa3Category.setInt(2, weekly_income);
            insertSa3Category.setString(3, gender);
            insertSa3Category.setInt(4, location);
            insertSa3Category.setString(5, income_source);
            insertSa3Category.executeUpdate();
            rs = insertSa3Category.getGeneratedKeys();
            if (rs.next()) {
                int tenantid = rs.getInt(1);
                return tenantid;
            } else {
                return 0;
                // throw an exception from here
            }
        } catch (SQLException sqlException) {
            System.out.println(sqlException);
            return 0;

        }
    }

    //get sa3tenant id
    public int getSa3Tenantid(int ageid, int weekly_incomeid, String gender, int locationid, String income_source) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            getSa3categoryId.setInt(1, ageid);
            getSa3categoryId.setInt(2, weekly_incomeid);
            getSa3categoryId.setString(3, gender);
            getSa3categoryId.setInt(4, locationid);
            getSa3categoryId.setString(5, income_source);
            rs = getSa3categoryId.executeQuery();
            if (rs.next()) {
                int tenantid = rs.getInt("Sa3CategoryId");
                //System.out.println(rs.getInt("locationId"));
                return tenantid;
            } else {
                return 0;
            }

        } catch (SQLException sqlException) {
            System.out.println(sqlException);
            return 0;

        }
    }

    // select all of the addresses in the database
    public LinkedList<SA3TenantCategory> getAllTenants(int tenantid) throws SQLException {
        // executeQuery returns ResultSet containing matching entries
        getAllSa3Category.setInt(1, tenantid);
        try (ResultSet resultSet = getAllSa3Category.executeQuery()) {
            LinkedList<SA3TenantCategory> results = new LinkedList<>();

            while (resultSet.next()) {
                results.add(new SA3TenantCategory(
                        resultSet.getString("agegroup"),
                        resultSet.getString("incomegroup"),
                        resultSet.getInt("locationid"),
                        resultSet.getString("gender"),
                        resultSet.getString("income_source")
                ));
            }

            return results;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return null;
    }

    // select all of the addresses in the database
    public LinkedList<SA3TenantCategory> getAllTenantsByLocation(String sa3code) throws SQLException {
        // executeQuery returns ResultSet containing matching entries
        getTenantBylocation.setString(1, sa3code);
        try (ResultSet resultSet = getTenantBylocation.executeQuery()) {
            LinkedList<SA3TenantCategory> results = new LinkedList<>();

            while (resultSet.next()) {
                results.add(new SA3TenantCategory(
                        resultSet.getString("agegroup"),
                        resultSet.getString("incomegroup"),
                        resultSet.getInt("locationid"),
                        resultSet.getString("gender"),
                        resultSet.getString("income_source")
                ));
            }

            return results;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return null;
    }

    // add data in Location Table
    public void addRiskyCount(int tenantid, int riskycount) {

        // insert the new entry; returns # of rows updated
        try {
            // set parameters
            insertRiskyCount.setInt(1, tenantid);
            insertRiskyCount.setInt(2, riskycount);
            insertRiskyCount.executeUpdate();
            System.out.println("RiskyCount Added Successfully");

        } catch (SQLException sqlException) {
            System.out.println(sqlException);

        }
    }
//update risky count
    public void updateRiskyCount(int Count, int tenantid) {
        try {
            updateRiskyCount.setInt(1, Count);
            updateRiskyCount.setInt(2, tenantid);
            updateRiskyCount.executeUpdate();
            System.out.println("RiskyCount updated");
        } catch (SQLException sqlException) {
            System.out.println(sqlException);

        }
    }

    // select all of the addresses in the database
    public LinkedList<RiskyPersons> getAllGender(String gender) throws SQLException {
        // executeQuery returns ResultSet containing matching entries
        ResultSet resultSet = null;
        if (gender == "male") {
            getallmalesdesc.setString(1, gender.trim());
            resultSet = getallmalesdesc.executeQuery();
        } else if (gender == "female") {
            getallfemalesdesc.setString(1, gender.trim());
            resultSet = getallfemalesdesc.executeQuery();
        } else if (gender == "%male%") {
            getallmalesdesc.setString(1, gender.trim());
            resultSet = getallmalesdesc.executeQuery();

        }
        try {
            LinkedList<RiskyPersons> results = new LinkedList<>();

            while (resultSet.next()) {
                results.add(new RiskyPersons(
                        resultSet.getInt("Sa3categoryID"),
                        resultSet.getInt("count")
                ));
            }

            return results;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return null;
    }

    // select all of the addresses in the database
    public LinkedList<RiskyPersons> getAllRiskyLocation(String sa3code) throws SQLException {
        // executeQuery returns ResultSet containing matching entries
        getriskycountbytenant.setString(1, sa3code);
        try (ResultSet resultSet = getriskycountbytenant.executeQuery()) {
            LinkedList<RiskyPersons> results = new LinkedList<>();

            while (resultSet.next()) {
                results.add(new RiskyPersons(
                        resultSet.getInt("Sa3categoryID"),
                        resultSet.getInt("count")
                ));
            }

            return results;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return null;
    }

    // close the database connection
    public void close() {

        try {
            connection.close();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
