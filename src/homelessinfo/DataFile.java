/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package homelessinfo;

import java.io.IOException;
import static java.lang.System.exit;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author preety
 * DataFile.java:::This class implements the file operations 
 */
public class DataFile {

    private static Scanner input;
    static LinkedList<RiskyPersons> riskyPersons = new LinkedList<>(); //create new linkedlist riskyperons
    static LinkedList<Location> locationlist = new LinkedList<>(); //create new linkedlist locationlist
    static LinkedList<SA3TenantCategory> sa3tenantlist = new LinkedList<>(); //create new linkedlist sa3tenantlist
    DatabaseUtility databasecon = new DatabaseUtility();

    public static void main(String[] args) throws SQLException {
        DataFile datafile = new DataFile();
        datafile.openFile(); // open file futureDemand.csv
        datafile.readRecords();//Read records from file
        datafile.closeFile();//close file
    }

    public DataFile() {

    }
    //open file
    public void openFile() { 
        try {
            input = new Scanner(Paths.get("src/homelessinfo/futureDemand.csv"));
        } catch (IOException ioException) {
            System.err.println("Error opening file. Terminating.");
            System.exit(1);
        }
    }

    // read record from file
    public void readRecords() throws SQLException {

        String line;
        String entries[];

        try {
            //databasecon.createAllTable();

            line = input.nextLine();
            String[] genders = line.split(",");

            line = input.nextLine();
            String[] ageGroups = line.split(",");

            line = input.nextLine();
            String[] employmentSources = line.split(",");

            while (input.hasNext()) { // while there is more to read
                line = input.nextLine(); //read each line
                entries = line.split(","); //split the line using the separator ,
                Location location = new Location(entries[0].trim(), entries[1].trim());
                locationlist.add(location);
                // databasecon.addlocation(entries[0].trim(), entries[1].trim());
                int locationid = databasecon.getLocationid(entries[0].trim());
                String incomeCategory = WEEKLY_INCOME.valueOf(this.getIncomeValue(entries[2])).toString();
                int weekly_incomeid = databasecon.getWeekly_Incomeid(entries[2].trim());

                for (int i = 3; i < 19; i++) {
                    String gender = GENDER.valueOf(genders[i]).toString();
                    String age = AGE.valueOf(this.getAgeValue(ageGroups[i])).toString();
                    int ageid = databasecon.getAgebyid(ageGroups[i].trim());
                    String incomeSource = employmentSources[i].trim();
                    int personCount = Integer.parseInt(entries[i]);

                    SA3TenantCategory sa3TenantCategory = new SA3TenantCategory(age, gender, locationid, incomeCategory,
                            incomeSource);
                    // int tenantid=databasecon.addTenantCategory(ageid, weekly_incomeid,genders[i], locationid, incomeSource);
                    //  RiskyPersons riskyPerson = new RiskyPersons(tenantid, personCount);
                    //  sa3tenantlist.add(sa3TenantCategory);
                    //  riskyPersons.add(riskyPerson);
                    //    databasecon.addRiskyCount(tenantid, personCount);
                }

            }

        } catch (NoSuchElementException elementException) {
            System.err.println("File improperly formed Terminating.");
        } catch (IllegalStateException stateException) {
            System.err.println("Error reading from file. Terminating.");
        }

    } // end method readRecords

    public String getIncomeValue(String income) {
        income = income.trim();
        switch (income) {
            case "Negative/Nil income":
                return "NILLINCOME";
            case "$1-$399":
                return "BELOW$400";
            case "$400-$599":
                return "BELOW$600";
            case "$600-$999":
                return "BELOW$1000";

            default:
                System.out.println("Invalid enum value");
        }
        return null;

    }
    //get enum value from WEEKLY_INCOME file

    public String getAgeValue(String age) {
        age = age.trim();
        switch (age) {
            case "50-54":
                return "UNDER55";
            case "55-59":
                return "BELOW60";
            case "60-64":
                return "BELOW65";
            case "over 65":
                return "OVER65";

            default:
                System.out.println("Invalid enum value");
        }
        return null;

    }
    //get age value from AGE file

    public void closeFile() {

        if (input != null) {
            input.close();
        }
    }
    //close the file

}
