/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homelessinfo;

/**
 *
 * @author preety
 */
public enum GENDER {
    Male("Male"),
    Female("Female");

    private final String GENDER;

    // enum constructor
    GENDER(String gender) {
        this.GENDER = gender;

    }

    // accessor for field AgeRange
    public String getGenderrange() {
        return GENDER;
    }

    @Override
    public String toString() {
        return String.format("%s", this.getGenderrange());
    }
    //Override enum GENDER object with toString

}
