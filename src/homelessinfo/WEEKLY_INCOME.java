/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homelessinfo;

/**
 *
 * @author preety
 */
public enum WEEKLY_INCOME {
    // declare constants of enum type   
    NILLINCOME("Negative/Nil"),
    BELOW$400("$1-$399"),
    BELOW$600("$400-$599"),
    BELOW$1000("$600-$999");

    // instance fields 
    private final String SALARYRANGE;

    // enum constructor
    WEEKLY_INCOME(String SALARYRANGE) {
        this.SALARYRANGE = SALARYRANGE;

    }

    public static WEEKLY_INCOME getWeeklyincome(String weeklyincome) {
        return WEEKLY_INCOME.valueOf(weeklyincome);
    }

    // accessor for field minSalary
    public String getSalaryrange() {
        return SALARYRANGE;
    }

    // accessor for field maxSalary
    @Override
    public String toString() {
        return String.format("%s", this.getSalaryrange());
    }
} // end enum Weekly_income

