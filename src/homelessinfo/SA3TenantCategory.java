/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homelessinfo;

/**
 *
 * @author preety
 */
public class SA3TenantCategory extends Person {

    private String income_source; // income source
    private final String income_category; //income category
    private final int location; //location variable

    public SA3TenantCategory(String agegroup, String gendergroup, int location, String income_category, String income_source) {
        super(agegroup, gendergroup);

        this.income_source = income_source;
        this.income_category = income_category;
        this.location = location;
    } //Parameterised COnstructor

    // set hours worked
    public void setSource(String income_source) {

        this.income_source = income_source;
    }

    // return hours worked
    public String getSource() {
        return income_source;
    }

    // return hours worked
    public int getlocation() {
        return location;
    }

    // return String representation of HourlyEmployee object              
    @Override
    public String toString() {
        return String.format("%d %s %-20s %-20s",
                location, super.toString(), income_category.toString(),
                this.getSource());
    }
} // end class HourlyEmployee

